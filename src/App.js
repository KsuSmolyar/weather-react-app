import styles from "./app.module.css";
import { Footer } from "./components/footer";
import { Header } from "./components/header";
import { Main } from "./components/main";

function App() {
	return (
		<div className={styles.app}>
			<Header />
			<Main />
			<Footer />
		</div>
	);
}

export default App;
