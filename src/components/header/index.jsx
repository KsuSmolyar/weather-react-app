import React from "react";
import styles from "./header.module.css";
import arrowUpd from "./arrowUpd.svg";

export const Header = () => {
	const options = {
		year: "numeric",
		month: "long",
		day: "numeric",
		weekday: "long",
	};
	const date = new Intl.DateTimeFormat("ru-RU", options).format(new Date());

	return (
		<header className={styles.header}>
			<div className={styles.headerContainer}>
				<div className={styles.headerDate}>{date}</div>
				<div className={styles.btnWrapper}>
					<button className={styles.switchUnits}>&deg;F</button>
					<button className={styles.btnUpdate}>
						<img src={arrowUpd} className={styles.arrowUpd} />
					</button>
				</div>
			</div>
		</header>
	);
};
