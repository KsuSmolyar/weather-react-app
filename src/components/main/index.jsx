import React, { useEffect } from "react";
import styles from "./main.module.css";
import locationIcon from "./locationIcon.svg";
import { useState } from "react";

export const Main = () => {
	const options = {
		hour: "numeric",
		minute: "numeric",
	};
	const time = new Intl.DateTimeFormat("ru-RU", options).format(Date.now());
	const [state, setState] = useState(null);
	useEffect(() => {
		// const coordinates =
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				const lon = position.coords.longitude;
				const lat = position.coords.latitude;
				fetch(
					`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=f33264c1a6e12720d231a8788bfeca2c&units=metric`
				)
					.then(function (response) {
						return response.json();
					})
					.then(function (data) {
						setState(data);
						console.log(data);
					})
					.catch(function () {
						//catch any errors
					});
			});
		} else {
			/* geolocation IS NOT available */
		}
	}, []);
	if (!state) {
		return <h1>Loading</h1>;
	}
	return (
		<main className={styles.main}>
			<div className={styles.mainWrapper}>
				<div className={styles.timeSections}>{time}</div>
				<div className={styles.citySelect}>
					<div className={styles.city}>
						<img src={locationIcon} className={styles.locationIcon} />
						<p>{state.name}</p>
					</div>
				</div>

				<div className={styles.weatherInfo}>
					<img
						className={styles.weatherIcon}
						src={`../../../../../img/${state.weather[0]["icon"]}.png`}
						alt={`${state.weather[0]["description"]}`}
					/>
					<div className={styles.weatherDescriptions}>
						{" "}
						<p className={styles.weatherUnitsDegrees}>
							{Math.round(state.main["temp"])}&deg;
						</p>
						<p>max {Math.round(state.main["temp_max"])}&deg; </p>
						<p>min {Math.round(state.main["temp_min"])}&deg; </p>
						<p>Humidity {state.main["humidity"]}%</p>
					</div>
				</div>
			</div>
		</main>
	);
};
