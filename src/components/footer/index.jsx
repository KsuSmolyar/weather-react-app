import React, { useEffect, useState } from "react";
import styles from "./footer.module.css";
import { WeatherColumn } from "./weatherColumn";

export const Footer = () => {
	const [state, setState] = useState(null);
	useEffect(() => {
		if ("geolocation" in navigator) {
			navigator.geolocation.getCurrentPosition((position) => {
				const lon = position.coords.longitude;
				const lat = position.coords.latitude;
				fetch(
					`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=f33264c1a6e12720d231a8788bfeca2c&units=metric&exclude=minutely,hourly`
				)
					.then(function (response) {
						return response.json();
					})
					.then(function (data) {
						setState(data);
						console.log(data);
					})
					.catch(function () {
						//catch any errors
					});
			});
		} else {
			/* geolocation IS NOT available */
		}
	}, []);
	if (!state) {
		return <h1>Loading</h1>;
	}

	const dayName = (date) =>
		new Intl.DateTimeFormat("en", {
			weekday: "long",
		}).format(date * 1000);

	const dayWeatherIcon = (icon) => {
		return icon;
	};
	console.log(state.daily);

	return (
		<footer className={styles.footer}>
			<div className={styles.footerWrapper}>
				{state.daily.slice(1, 4).map((item) => (
					<WeatherColumn
						dayName={dayName(item.dt)}
						dayWeatherIcon={item.weather[0].icon}
						description={item.weather[0].description}
						tempDay={item.temp.day}
						tempNight={item.temp.night}
						key={item.dt}
					/>
				))}
			</div>
		</footer>
	);
};
