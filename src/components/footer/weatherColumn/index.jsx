import React from "react";
import styles from "./weatherColumn.module.css";

export const WeatherColumn = ({
	dayName,
	dayWeatherIcon,
	description,
	tempDay,
	tempNight,
}) => {
	return (
		<div className={styles.weatherColumnWrapper}>
			<div className={styles.weatherColumn}>
				<p className={styles.columnTitles}>{dayName}</p>
				<img
					className={styles.footerWeatherIcon}
					src={`../../../../../img/${dayWeatherIcon}.png`}
					alt={description}
					title={description}
				/>
				<div className={styles.footerWeatherDegrees}>
					{Math.round(tempDay)}&deg;/
					{Math.round(tempNight)}&deg;
				</div>
			</div>
		</div>
	);
};
